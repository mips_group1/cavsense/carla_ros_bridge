# Carla ROS Framework

## Installation

It is based on https://github.com/carla-simulator/ros-bridge



    Create a catkin workspace and install carla_ros_bridge package

    #setup folder structure
    mkdir -p ~/carla-ros-bridge/catkin_ws/src
    cd ~/carla-ros-bridge
    git clone https://gitlab.com/isi_athena_rc/cavsense/carla_ros_bridge.git
    cd carla_ros_bridge
    git submodule update --init
    cd ../catkin_ws/src
    ln -s ../../rcarla_ros_bridge
    source /opt/ros/<kinetic or melodic or noetic>/setup.bash
    cd ..

    #install required ros-dependencies
    rosdep update
    rosdep install --from-paths src --ignore-src -r

    #build
    catkin_make



## Execution

The whole simulation procedure can be initiated by a single roslaunch command by starting test_scenario node and the appropriate launch file. 
Currently, there is only one file (carla_ad_demo_Town10.launch).

First run the simulator (see carla documentation: <http://carla.readthedocs.io/en/latest/>)

    # run carla in background
    SDL_VIDEODRIVER=offscreen ./CarlaUE4.sh -opengl

Wait a few seconds

    export PYTHONPATH=$PYTHONPATH:<path-to-carla>/PythonAPI/carla/dist/carla-<carla_version_and_arch>.egg


    source ~/carla-ros-bridge/catkin_ws/devel/setup.bash

Start the test scenario:

    # roslaunch test_scenario carla_ad_demo_Town10.launch calibration_speed:=20 target_speed:=40



## Launch File
The launch file contains information about ros nodes and their parameters.
More specifically, it starts and provides configuration information for the following nodes. 


### ros bridge 

It is the core of the simulation framework. It orchestrates and implements the bidirectional communication between carla simulation world and ROS runtime. The file contains the following parameters. 



| Name                              | Description                                                        |Simulation Value|
| --------------------------------- | ------------------------------------------------------------------ |----------------|
| Host|Ip of Carla server|localhost|
| Port|Port of the carla server |2000|
| Town|The carla map to load |Map10HD |
| Timeout|The carla timeout for server communicatio|10|
| Passive|The ROS bridge stays passive and other clients have the responsibility of ticking and configuring the world |False|
| synchronous_mode|Whether the server or a client leads the simulation|false|
| synchronous_mode_wait_for_vehicle_control_command |should the ros bridge wait for a vehicle control command before proceeding with the next tick |false|
| fixed_delta_seconds|frequency of the carla ticks |0.033 (30fps) |





### ego  vehicle 

It is the main actor in the scene, to which the various sensors are attached. It follows a predefined path. Its movement is split into two phases. In the first phase, the calibration phase, the vehicle moves slowly (~20km/h) for acquiring data, which are used for aligning the estimated paths from DSO and LegoLOAM to ground truth. Currently, this is implemented only for DSO. The second and last phase is the running phase, in which the actual data are collected. The node contains the following parameters.   




| Name                              | Description                                                        |Simulation Value|
| --------------------------------- | ------------------------------------------------------------------ |----------------|
|objects_definition_file |This file contains in json format all the sensors that we’ll be attached to the vehicle |path_to_ros_bridge/carla_spawn_objects/config/objects.json |
|role_name |Name of the ego vehicle |ego_vehicle |
|spawn_point_ego_vehicle|Spawn point of the ego vehicle in the map |‘60.7,-131.7,0.1,0,0,180’|
|spawn_sensors_only |Spawn only the sensors|false |
 

### Test Vehicle 

It’s the entity responsible for controlling the ego_vehicle by issuing commands for following a predefined path and for controlling the simulation weather. It has the following parameters. 

| Name                              | Description                                                        |Simulation Value|
| --------------------------------- | ------------------------------------------------------------------ |----------------|
|target_speed |The target speed of ego vehicle |40km/h |
|role_name |The name of the ego vehcle |ego_vehicle|
|avoid_risk |Whether to respect traffic lights and avoid collisions|No (if no other vehicles are in the simulation| 
|calibration_target |End point of calibration phase |{ 'pose': { 'position': { 'x': -23.112, 'y': -131.7, 'z': 0.0 }, 'orientation': { 'x': -1.30534, 'y': 3.429521, 'z': 0.9999999, 'w': 0.00013928 } } }|
|calibration_speed |Target speed during calibration phase|20kmh |
|waypoints|Path the ego vehicle must follow. It consists of waypoint|{'point1':{ 'pose': { 'position': { 'x': -44.9768089, 'y': -107.46, 'z': 0.0 }, 'orientation': { 'x': -0, 'y': -0, 'z': 0.70905, 'w': 0.70515744 } } },'point2':{ 'pose': { 'position': { 'x': -45.4602, 'y': -46.60, 'z': 0.0 }, 'orientation': { 'x': 0.0, 'y': 0, 'z': 0.7082, 'w': 0.706 } } },'point3':{ 'pose': { 'position': { 'x': -59.1931953, 'y': 59.1043, 'z': 0.0 }, 'orientation': { 'x': 0, 'y': -4.775, 'z': 0.02989, 'w': 0.99955 } } },'point4':{ 'pose': { 'position': { 'x': 98.14645, 'y': -13.268, 'z': 0.0 }, 'orientation': { 'x': -0, 'y': 0, 'z': -0.7, 'w': 0.682 } } },'point5':{ 'pose':  { 'position': { 'x': -23.112, 'y': -131.68651, 'z': 0.0 }, 'orientation': { 'x': -1.30534, 'y': 3.429521, 'z': 0.9999999, 'w': 0.00013928 } } },'point6':{ 'pose': { 'position': { 'x': -59.1931953, 'y': 59.1043, 'z': 0.0 }, 'orientation': { 'x': 0, 'y': -4.775, 'z': 0.02989, 'w': 0.99955 } } },'point7':{ 'pose': { 'position': { 'x': -52.45, 'y': 4.7, 'z': 0.0 }, 'orientation': { 'x': 0, 'y': 0.0001, 'z': -0.69, 'w': 0.71 } } },'point8':{ 'pose': { 'position': { 'x': -59.1931953, 'y': -114.4, 'z': 0.0 }, 'orientation': { 'x': 0, 'y': 0, 'z': -0.78, 'w': 0.627 } } },'point9':{ 'pose': { 'position': { 'x': -104.86, 'y': -72.36, 'z': 0.0 }, 'orientation': { 'x': 0, 'y': 0.0, 'z': 0.717, 'w':0.696 } } },'point10':{ 'pose': { 'position': { 'x': -72.408, 'y': -28.334, 'z': 0.0 }, 'orientation': { 'x': 0, 'y': 0, 'z': -0.006, 'w': 1 } } },'point11':{ 'pose': { 'position': { 'x': -44.79, 'y': -16.725, 'z': 0.0 }, 'orientation': { 'x': 0, 'y': 0, 'z': -0.706, 'w': 0.708 } } }} |
|sun|Defines the sun light |sun or night |
|weather|Defines the weather. |clear or rain|


### waypoint publisher 

It provides a route and access to the CARLA waypoint API. It is required by the previous entity, the agent. 

| Name                              | Description                                                        |Simulation Value|
| --------------------------------- | ------------------------------------------------------------------ |----------------|
|Host|The target speed of ego vehicle |Ip of Carla server|localhost|
| Port|Port of the carla server |2000|
| Timeout|The carla timeout for server communicatio|10|
|role_name |Name of the ego vehicle |ego_vehicle |
 


### manual_control 

It visualizes the ego vehicle and supports overriding the control. 

 

| Name                              | Description                                                        |Simulation Value|
| --------------------------------- | ------------------------------------------------------------------ |----------------
|role_name |Name of the ego vehicle |ego_vehicle |
 

### Ros-dso node 

It starts the dso algorithm which takes as input the image produced by the camera attached in the front view of the ego vehicle. It has the following parameters. 

| Name                              | Description                                                        |Simulation Value|
| --------------------------------- | ------------------------------------------------------------------ |----------------
|Calib |The path to the camera calibration file |path_to_the_calib_file/calib.txt  |
|Image|The image topic |/carla/ego_vehicle/rgb_front/image |
|dso_start_delay|Delays the start of the node |10 second |
|disableAllDisplay |Disables Pangolin |False|


 

### Lego-loam node 

It spawns the lego loam related nodes.  The nodes, which are included in the lego Loam launch file, correspond to the distinct functions of the algorithm and are the imageProjection, the featureAssociation, the mapOptimization, and the transformFusion. Moreover, it spawns the rviz for visualization purposes. 

### logger node 

This node collects data from topics of interest during running phase and stores them in csv files. 

| Name                              | Description                                                        |Simulation Value|
| --------------------------------- | ------------------------------------------------------------------ |----------------
|folder |Name of the storing location |path_to_the_folder |

