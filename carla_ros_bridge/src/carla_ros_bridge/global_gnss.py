#!/usr/bin/env python

#
# Copyright (c) 2018-2019 Intel Corporation
#
# This work is licensed under the terms of the MIT license.
# For a copy, see <https://opensource.org/licenses/MIT>.
#
"""
Class to handle the gnss sensors of the world
"""

import rospy

from test_msgs.msg import GNSSActorList



class GlobalGNSS(object):

    """
    Publish the map
    """

    def __init__(self, uid, name, parent, node, vehicle_list, carla_world):
        """
        Constructor

        :param carla_world: carla world object
        :type carla_world: carla.World
        """
        self.world=carla_world
        self.world_gnss_publisher= rospy.Publisher("/carla/vehicles/gnss",
                                                    GNSSActorList,
                                                    queue_size=10,
                                                    latch=True)

    def destroy(self):
        """
        Function (override) to destroy this object.

        Remove reference to carla.Map object.
        Finally forward call to super class.

        :return:
        """
        rospy.logdebug("Destroying GNSSActorList()")
        
        
    @staticmethod
    def get_blueprint_name():
        """
        Get the blueprint identifier for the pseudo sensor
        :return: name
        """
        return "sensor.pseudo.global_gnss"

    def update(self, frame, timestamp):
        """
        Function (override) to update this object.

        :return:
        """
        print("GNSSS")