#!/usr/bin/env python
#
# Copyright (c) 2020 Intel Corporation
#
# This work is licensed under the terms of the MIT license.
# For a copy, see <https://opensource.org/licenses/MIT>.
#
"""
A basic AD agent using CARLA waypoints
"""
import sys


#path to ros-bridge directory
sys.path.append("/home/chranagno/carla-ros-bridge/ros-bridge")


import rospy
from nav_msgs.msg import Path
from geometry_msgs.msg import PoseStamped
from std_msgs.msg import Float64
from std_msgs.msg import String
from carla_msgs.msg import CarlaEgoVehicleInfo, CarlaEgoVehicleControl,CarlaWeatherParameters
from MyAgent import MyAgent  # pylint: disable=relative-import
import json
from collections import OrderedDict




SUN_PRESETS = {
    'day': (60.0, 0.0),
    'night': (-90.0, 0.0),
    'sunset': (0.5, 180.0)}

WEATHER_PRESETS = {
    'clear': [10.0, 0.0, 0.0, 5.0, 0.0, 0.0, 0.2, 0.0],
    'overcast': [80.0, 0.0, 0.0, 50.0, 2.0, 0.0, 0.9, 10.0],
    'rain': [100.0, 80.0, 90.0, 100.0, 20.0, 0.0, 0.9, 100.0]}


class CarlaAdAgent(object):
    """
    A basic AD agent using CARLA waypoints
    """

    def __init__(self, role_name, target_speed, avoid_risk, targets, calibration_speed,sun,weather,respect_traffic_lights):
        """
        Constructor
        """
        self.published_current_target=False
        self._route_assigned = False
        self._global_plan = None
        self._agent = None
        self._calibration_speed=calibration_speed
        self._target_speed = target_speed
        self._final_speed=self._calibration_speed
        self.calibrated=False
        self.targets=targets
        self.ego_vehicle_state="IDLE"
      #  self._calibration_speed=calibration_speed
        #self._waypoints=waypoints
        rospy.on_shutdown(self.on_shutdown)
        
        #Arrange weather
        self.weather=CarlaWeatherParameters()
        self._weather_publisher=rospy.Publisher("/carla/weather_control",CarlaWeatherParameters,queue_size=1)
        self.weather_apply_presets(weather, sun)
       
        

        # wait for ego vehicle
        vehicle_info = None
        try:
            vehicle_info = rospy.wait_for_message(
                "/carla/{}/vehicle_info".format(role_name), CarlaEgoVehicleInfo)
        except rospy.ROSException:
            rospy.logerr("Timeout while waiting for world info!")
            sys.exit(1)



        self._route_subscriber = rospy.Subscriber(
            "/carla/{}/waypoints".format(role_name), Path, self.path_updated)

        self._target_speed_subscriber = rospy.Subscriber(
            "/carla/{}/target_speed".format(role_name), Float64, self.target_speed_updated)

        self.vehicle_control_publisher = rospy.Publisher(
            "/carla/{}/vehicle_control_cmd".format(role_name), CarlaEgoVehicleControl, queue_size=1)
        self.goal_publisher=rospy.Publisher(
                "/carla/{}/goal".format(role_name), PoseStamped, latch=True, queue_size=1)
        
        self.ego_state_publisher=rospy.Publisher(
                "carla/{}/state".format(role_name), String, queue_size=1)

        self._agent = MyAgent(role_name, vehicle_info.id,  # pylint: disable=no-member
                                 avoid_risk, respectTrafficLights=respect_traffic_lights)
        self.current_target=targets.pop(0)
     
       

    def on_shutdown(self):
        """
        callback on shutdown
        """
        rospy.loginfo("Shutting down, stopping ego vehicle...")
        if self._agent:
            self.vehicle_control_publisher.publish(self._agent.emergency_stop())

    def target_speed_updated(self, target_speed):
        """
        callback on new target speed
        """
        rospy.loginfo("New target speed received: {}".format(target_speed.data))
        self._target_speed = target_speed.data
        
    def weather_apply_presets(self, weather, sun):
        print(weather)
        print(sun)
        if (weather and sun) is not None:
            if weather in WEATHER_PRESETS and sun in SUN_PRESETS:
                print(WEATHER_PRESETS[weather])
                self.weather.cloudiness = WEATHER_PRESETS[weather][0]
                self.weather.precipitation = WEATHER_PRESETS[weather][1]
                self.weather.precipitation_deposits = WEATHER_PRESETS[weather][2]
                self.weather.wind_intensity = WEATHER_PRESETS[weather][3]
                self.weather.fog_density = WEATHER_PRESETS[weather][4]
                self.weather.fog_distance = WEATHER_PRESETS[weather][5]
                self.weather.wetness = WEATHER_PRESETS[weather][7]
                
                
                print(SUN_PRESETS[sun])
                self.weather.sun_azimuth_angle=SUN_PRESETS[sun][0]
                self.weather.sun_altitude_angle=SUN_PRESETS[sun][1]
                
                print(self.weather)
                

        
                
           
           
    def path_updated(self, path):
        """
        callback on new route
        """
        
        rospy.loginfo("New plan with {} waypoints received.".format(len(path.poses)))
        if self._agent:
            self.vehicle_control_publisher.publish(self._agent.emergency_stop())
        self._global_plan = path
        self._route_assigned = False

    def run_step(self):
        """
        Execute one step of navigation.
        """
        control = CarlaEgoVehicleControl()
        control.steer = 0.0
        control.throttle = 0.0
        control.brake = 0.0
        control.hand_brake = False
        
        if self.ego_vehicle_state=="RUNNING":
            self._final_speed=self._target_speed

        if not self._agent:
            rospy.loginfo("Waiting for ego vehicle...")
            return control

        if not self._route_assigned and self._global_plan:
            rospy.loginfo("Assigning plan...")
            self._agent._local_planner.set_global_plan(  # pylint: disable=protected-access
                self._global_plan.poses)
            self._route_assigned = True
        else:
            control, finished = self._agent.run_step(self._final_speed)
            if finished:
                print("Target: {0} reached".format(self.current_target))
                #print(self.targets)
                if(len(self.targets)>0):
                    self.current_target=self.targets.pop(0)
                    self.published_current_target=False
#                    self.goal_publisher.publish(self.current_target)
                    #print(self.current_target)
                else:
                    print("FINISHED!!")
                    self.current_target=None
                    self.ego_vehicle_state="FINISHED"
                    #rospy.signal_shutdown("Everything went OK!")
                    
                
                self._global_plan = None
                self._route_assigned = False
                

        return control

    def run(self):
        """

        Control loop

        :return:
        """
        r = rospy.Rate(10)
       
        while not rospy.is_shutdown():
            self.ego_state_publisher.publish(self.ego_vehicle_state)
           # self._weather_publisher.publish(self.weather)
            if(self.published_current_target==False):
                self.goal_publisher.publish(self.current_target)
                
                print("Target x: {0}, y: {1}".format(self.current_target.pose.position.x,self.current_target.pose.position.y ))
                if self.ego_vehicle_state=="IDLE":
                    self.ego_vehicle_state="CALIB_X"
                elif self.ego_vehicle_state=="CALIB_X":
                    self.ego_vehicle_state="X_TO_Y"
                elif self.ego_vehicle_state=="X_TO_Y":
                    self.ego_vehicle_state="CALIB_Y"            
                else:
                    self.ego_vehicle_state="RUNNING"
                
                        
                    
                self.published_current_target=True;
          
            if self._global_plan:
                control = self.run_step()
                if control:
                    control.steer = -control.steer
                    self.vehicle_control_publisher.publish(control)
            else:
                try:
                    r.sleep()
                except rospy.ROSInterruptException:
                    pass
                
                
def getPose( dic):
    
    pose=PoseStamped()
    pose.pose.position.x= dic["position"]["x"]
    pose.pose.position.y= dic["position"]["y"]
    pose.pose.position.z= dic["position"]["z"]
    pose.pose.orientation.x= dic["orientation"]["x"]
    pose.pose.orientation.y= dic["orientation"]["y"]
    pose.pose.orientation.z= dic["orientation"]["z"]
    pose.pose.orientation.w= dic["orientation"]["w"]
    pose.header.frame_id = "map"
    pose.header.stamp=rospy.Time.now()
    return pose


def main():
    """

    main function

    :return:
    """
    
    targets=[]
    rospy.init_node('test_vehicle', anonymous=True)
    role_name = rospy.get_param("~role_name", "ego_vehicle")
    target_speed = rospy.get_param("~target_speed", 20)
    avoid_risk = rospy.get_param("~avoid_risk", True)
    calibration_speed=rospy.get_param("~calibration_speed", 30)
    calibration_target=rospy.get_param("~calibration_target")
    respect_traffic_lights=rospy.get_param("~respect_traffic_lights", False)
    sun=rospy.get_param("~sun","day")
    weather=rospy.get_param("~weather","clear")

    calibration_target=json.loads(calibration_target.replace("'", "\""))
    targets.append(getPose(calibration_target["pose"]))
    waypoints_str=rospy.get_param("~waypoints")
    if (waypoints_str!=""):
        #use OrderedDict for keeping the order of the dic
        waypoints=json.loads(waypoints_str.replace("'", "\""), object_pairs_hook=OrderedDict)

        for w in waypoints:
            pose=getPose(waypoints[w]["pose"])
            targets.append(pose)


    controller = CarlaAdAgent(role_name, target_speed, avoid_risk, targets, calibration_speed,sun,weather,respect_traffic_lights)
    try:
        controller.run()
    finally:
        del controller
        rospy.loginfo("Done")


if __name__ == "__main__":
    main()
